import Swiper from 'swiper';

export default function (selector) {

  return new Swiper(selector,
    {
      preloadImages: false,
      lazy: true,
      watchSlidesVisibility: true,
      loadPrevNext: true,
      navigation: {
        nextEl: '.shows-button-next',
        prevEl: '.shows-button-prev',
      },
      breakpoints: {
        // when window width is >= 100px
        100: {
          slidesPerView: 1,
          freeMode: true,
        },
        // when window width is >= 768px
        768: {
          slidesPerView: 2,
          freeMode: true,
        },
        // when window width is >= 991px
        991: {
          slidesPerView: 3,
          freeMode: true,
        },
      },
    });
}

